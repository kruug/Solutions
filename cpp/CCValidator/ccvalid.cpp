#include <iomanip>
#include <iostream>
#include <math.h>
#include <stdlib.h>
// ccvalid.cpp
// Verify validity of user-inputted credit card.
 
int main(void)
{
  int card[15];
  int card_reverse[15];
  int card_number;
  int card_total;
  int check_sum;
  std::string card_status;
  std::string issuer;
  std::string temp_card[16];
  std::string user_card;

  std::cout << "Please enter your credit card number ";
  std::cin >> user_card;

  if (user_card.length() < 16)
  {
    std::cout << "That number is too short." << std::endl;
    return 0;
  }

  if (user_card.length() > 16)
  {
    std::cout << "That number is too long." << std::endl;
    return 0;
  }

  if (user_card.substr(0,1) == "1")
  {
    if (user_card.substr(0,4) == "1800")
    {
      issuer = "JCB";
    } else {
      issuer = "This card is not valid";
    }
  } else if (user_card.substr(0,1) == "2") {
    if (user_card.substr(0,4) == "2014")
    {
      issuer = "Diner's Club / enRoute";
    } else if (user_card.substr(0,4) == "2131") {
      issuer = "JCB";
    } else if (user_card.substr(0,4) == "2149") {
      issuer = "Diner's Club / enRoute";
    } else {
      issuer = "This card is not valid";
    }
  } else if (user_card.substr(0,1) == "3") {
    if (user_card.substr(0,3) == "300" || user_card.substr(0,3) == "301" || user_card.substr(0,3) == "302" || user_card.substr(0,3) == "303" || user_card.substr(0,3) == "304" || user_card.substr(0,3) == "305")
    {
      issuer = "Diner's Club / Carte Blanche";
    } else if (user_card.substr(0,2) == "34") {
      issuer = "American Express";
    } else if (user_card.substr(0,2) == "36") {
      issuer = "Diner's Club / International";
    } else if (user_card.substr(0,2) == "37") {
      issuer = "American Express";
    } else if (user_card.substr(0,2) == "38") {
      issuer = "Diner's Club / Carte Blanche";
    } else {
      issuer = "JCB";
    }
  } else if (user_card.substr(0,1) == "4") {
    issuer = "Visa";
  } else if (user_card.substr(0,2) == "51" || user_card.substr(0,2) == "52" || user_card.substr(0,2) == "53" || user_card.substr(0,2) == "54" || user_card.substr(0,2) == "55"){
    issuer = "MasterCard";
  } else if (user_card.substr(0,4) == "6011") {
    issuer = "Discover";
  } else {
    issuer = "This card is not valid";
  }

  int i = 0;
  while ( i < 16 )
  {
    card[i] = atoi(user_card.substr(i,1).c_str());
    i++;
  }

  check_sum = atoi(user_card.substr(15,1).c_str());

  for (int i = 0; i < 15; i++)
  {
    card_reverse[14 - i] = card[i];
  }

  for (int i = 0; i < 15; i += 2)
  {
    card_reverse[i] *= 2;
  }

  for (int i = 0; i < 15; i++)
  {
    if (card_reverse[i] > 9) {
      card_reverse[i] -= 9;
    }
  }

  for (int i = 0; i < 15; i++)
  {
    card_total += card_reverse[i];
  }

  if ((card_total % 10) == check_sum)
  {
    card_status = "This is a valid credit card number";
  } else {
    card_status = "This credit card number did not past the checksum test.  It is invalid";
  }

  std::cout << "Issuer: " << issuer << std::endl;
  std::cout << user_card.substr(0,4) << " " << user_card.substr(4,4) << " " << user_card.substr(8,4) << " " << user_card.substr(12,4) << std::endl;

  std::cout << card_status;

  std::cout << std::endl;
  return 0;
}
