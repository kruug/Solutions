#include <iostream>
#include <math.h>
#include <iomanip>
// fibonacci.cpp
// Calculate and return the Fibonacci number to the 
//   number of digits selected by the user.
 
int main(void)
{
  int first  = 0;
  int second = 1;
  int third  = 1;

  int user_length;

  std::cout << "How many digits should I return (max 20)? ";
  std::cin >> user_length;

  if (user_length > 20) {
    user_length = 20;
  }

  std::cout << first  << " ";
  std::cout << second << " ";

  for (int i = 0; i < user_length - 2; i++)
  {
    third = first + second;
    first = second;
    second = third;

    std::cout << third << " ";
  }

  std::cout << std::endl;

  return 0;
}
