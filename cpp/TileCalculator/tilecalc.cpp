#include <iostream>
#include <math.h>
#include <iomanip>
// tilecalc.cpp
// Calculate how much it will cost to tile a room
//   with user provided dimensions
 
int main(void)
{
  int room_size;
  int user_length;
  int user_width;
  float total_cost;
  float user_cost;

  std::cout << "How wide is your room? ";
  std::cin >> user_width;
  std::cout << "How long is your room? ";
  std::cin >> user_length;
  std::cout << "How much is your tile per square? ";
  std::cin >> user_cost;

  room_size = user_length * user_width;

  std::cout << "Room size: " << room_size << std::endl;

  total_cost = room_size * user_cost;

  std::cout << "Total cost: " << total_cost << std::endl;

  return 0;
}
