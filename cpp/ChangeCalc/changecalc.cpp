#include <iostream>
#include <math.h>
#include <iomanip>
// changecalc.cpp
// Calculate and return how much change and what bills/coins should be
//   returned to the user.
 
int main(void)
{
  float user_cash;
  float user_change;
  float user_total;
  int change_bills;
  int change_coins;
  int dimes;
  int dollars;
  int fives;
  int nickels;
  int pennies;
  int quarters;
  int tens;
  int twenties;

  std::cout << "What is the total? ";
  std::cin >> user_total;
  std::cout << "What is the cash? ";
  std::cin >> user_cash;

  user_change = user_cash - user_total;
  change_bills = user_change;
  change_coins = (((user_change - change_bills) * 10) * 10);

  std::cout << "The change is $" << std::fixed << std::setprecision(2) << user_change << std::endl;
  
  twenties = change_bills / 20;
  change_bills = change_bills - (twenties * 20);
  tens = change_bills / 10;
  change_bills = change_bills - (tens * 10);
  fives = change_bills / 5;
  change_bills = change_bills - (fives * 5);
  dollars = change_bills / 1;
  change_bills = change_bills - (dollars * 1);

  quarters = change_coins / 25;
  change_coins = change_coins - (quarters * 25);
  dimes = change_coins / 10;
  change_coins = change_coins - (dimes * 10);
  nickels = change_coins / 5;
  change_coins = change_coins - (nickels * 5);
  pennies = change_coins / 1;
  change_coins = change_coins - (pennies * 1);

  std::cout << "Twenties: \t" << twenties << "\t$" << twenties * 20 << std::endl;
  std::cout << "Tens: \t\t" << tens << "\t$" << tens * 10 << std::endl;
  std::cout << "Fives: \t\t" << fives << "\t$" << fives * 5 << std::endl;
  std::cout << "Dollars: \t" << dollars << "\t$" << dollars << std::endl;

  std::cout << "Quarters: \t" << quarters << "\t$" << quarters * 0.25 << std::endl;
  std::cout << "Dimes: \t\t" << dimes << "\t$" << dimes * 0.10 << std::endl;
  std::cout << "Nickels: \t" << nickels << "\t$" << nickels * 0.05 << std::endl;
  std::cout << "Pennies: \t" << pennies << "\t$" << pennies * 0.01 << std::endl;
  return 0;
}
