#include <iostream>
#include <math.h>
#include <iomanip>
// mortgagecalc.cpp
// Calculate the monthly payments of a fixed term mortgage with values
//   provided by user
 
int main(void)
{
  float denominator;
  float denominator_power;
  float monthly_interest;
  float monthly_payment;
  float number_of_payments_months;
  float number_of_payments_years;
  float numerator;
  float numerator_power;
  float principal;
  float total_interest_paid;
  float total_money_paid;
  float yearly_interest;

  std::cout << "What is the mortgage principal? ";
  std::cin >> principal;
  std::cout << "What is the annual interest rate? ";
  std::cin >> yearly_interest;
  std::cout << "How many years is the mortgage? ";
  std::cin >> number_of_payments_years;

  number_of_payments_months = number_of_payments_years * 12;
  monthly_interest = yearly_interest / 12;

  monthly_payment = (principal * (monthly_interest * pow((1 + monthly_interest), number_of_payments_months))) / (pow((1 + monthly_interest), number_of_payments_months) - 1);

  total_money_paid = monthly_payment * number_of_payments_months;
  total_interest_paid = total_money_paid - principal;

  std::cout << "Your monthly payment: " << monthly_payment << std::endl;
  std::cout << "Your total money paid: " << total_money_paid << std::endl;
  std::cout << "Your total interest paid: " << total_interest_paid << std::endl;
  return 0;
}
