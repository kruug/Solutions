#include <iostream>
#include <math.h>
#include <iomanip>
// picalc.cpp
// Calculate and return pi to the number of digits
//   selected by the user.
 
int main(void)
{
  // Calculate pi
  long double calc_pi  = ((16 * atan(1. / 5)) - (4 * atan(1. / 239)));

  int user_length;

  std::cout << "How many digits should I return (max 20)? ";
  std::cin >> user_length;

  if (user_length > 20) {
    user_length = 20;
  }

  std::cout << std::setprecision(user_length + 1) << calc_pi << std::endl;
  return 0;
}
