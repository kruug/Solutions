from tkinter import *
from tkinter import messagebox

root = Tk()

def stopProg(e):
  root.destroy()

def validate(e):
  intCCNumber = re.sub('[^0-9]+', '', txtCCNumber.get("1.0",END))

  if (len(intCCNumber) < 16):
    messagebox.showerror('Error', 'Card number too short')
  if (len(intCCNumber) > 16):
    messagebox.showerror('Error', 'Card number too long')
  
  ccNumber.set(int(intCCNumber))

ccNumber = StringVar()

txtCCNumber = Text(root, height=1, width=20)
btnValidate = Button(root, text="Validate")
lblNumber = Label(root, textvariable=ccNumber)

txtCCNumber.pack()
btnValidate.pack()
lblNumber.pack()

btnValidate.bind('<Button-1>', validate)

root.mainloop()